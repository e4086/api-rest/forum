INSERT INTO TB_USUARIO(us_nome, us_email, us_senha) VALUES('Aluno', 'aluno@email.com', '$2a$10$6JCTuO8f7BCGaJL/wIlpZuIsVjiil0G82Fnr/2f7g8hy.v9J4KfbK');

INSERT INTO TB_CURSO(cu_nome, cu_categoria) VALUES('Spring Boot', 'Programação');
INSERT INTO TB_CURSO(cu_nome, cu_categoria) VALUES('HTML 5', 'Front-end');

INSERT INTO TB_TOPICO(to_titulo, to_mensagem, to_data_criacao, to_status, us_id, cu_id) VALUES('Dúvida', 'Erro ao criar projeto', '2019-05-05 18:00:00', 'NAO_RESPONDIDO', 1, 1);
INSERT INTO TB_TOPICO(to_titulo, to_mensagem, to_data_criacao, to_status, us_id, cu_id) VALUES('Dúvida 2', 'Projeto não compila', '2019-05-05 19:00:00', 'NAO_RESPONDIDO', 1, 1);
INSERT INTO TB_TOPICO(to_titulo, to_mensagem, to_data_criacao, to_status, us_id, cu_id) VALUES('Dúvida 3', 'Tag HTML', '2019-05-05 20:00:00', 'NAO_RESPONDIDO', 1, 2);
package com.estudo.forum.dto;

import com.estudo.forum.modelo.Resposta;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespostaDto {

    private Long id;

    private String mensagem;

    private LocalDateTime dataCriacao = LocalDateTime.now();

    private String nomeAutor;

    public RespostaDto(Resposta resposta) {
        this.id = resposta.getId();
        this.mensagem = resposta.getMensagem();
        this.nomeAutor = resposta.getAutor().getUsername();
    }
}

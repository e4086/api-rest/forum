package com.estudo.forum.dto;

import com.estudo.forum.modelo.StatusTopico;
import com.estudo.forum.modelo.Topico;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Getter
public class DetalhesTopicoDto {

    private Long id;

    private StatusTopico status;

    private String titulo;

    private String mensagem;

    private LocalDateTime dataCriacao;

    private final String nomeAutor;

    private final List<RespostaDto> respostas;

    public DetalhesTopicoDto(Topico topico) {
        this.id = topico.getId();
        this.status = topico.getStatus();
        this.titulo = topico.getTitulo();
        this.mensagem = topico.getMensagem();
        this.dataCriacao = topico.getDataCriacao();

        this.nomeAutor = nonNull(topico.getAutor()) ? topico.getAutor().getUsername() : null;
        this.respostas = topico.getRespostas().stream().map(RespostaDto::new).collect(Collectors.toList());
    }
}

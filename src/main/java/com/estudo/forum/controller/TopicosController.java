package com.estudo.forum.controller;

import com.estudo.forum.dto.DetalhesTopicoDto;
import com.estudo.forum.dto.TopicoDto;
import com.estudo.forum.form.TopicoForm;
import com.estudo.forum.form.TopicoFormEdicao;
import com.estudo.forum.modelo.Topico;
import com.estudo.forum.repository.CursoRepository;
import com.estudo.forum.repository.TopicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

import static java.util.Objects.isNull;

@RestController
@RequestMapping("/topicos")
public class TopicosController {

    @Autowired
    private TopicoRepository topicoRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @GetMapping
    @Cacheable(value = "listaTopicos")
    public Page<TopicoDto> buscarTodos(@RequestParam(required = false) String nomeCurso,
                                       @PageableDefault(sort = "id", direction = Sort.Direction.ASC) Pageable pageable) {


        Page<Topico> pageTopicos;

        if (isNull(nomeCurso)) {
            pageTopicos = topicoRepository.findAll(pageable);
        } else {
            pageTopicos = topicoRepository.findByCursoNome(nomeCurso, pageable);
        }

        return TopicoDto.fromTopicosPage(pageTopicos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DetalhesTopicoDto> buscarPorId(@PathVariable("id") Long id) {

        Optional<Topico> topicoOptional = topicoRepository.findById(id);

        if (topicoOptional.isPresent()) {
            return ResponseEntity.ok(new DetalhesTopicoDto(topicoOptional.get()));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping
    @CacheEvict(value = "listaTopicos", allEntries = true)
    public ResponseEntity<TopicoDto> salvar(@RequestBody @Valid TopicoForm topicoForm, UriComponentsBuilder uriBuilder) {

        Topico topico = topicoForm.toTopico();

        topico.setCurso(cursoRepository.findByNome(topicoForm.getNomeCurso()));

        topicoRepository.save(topico);

        URI uri = uriBuilder.path("/topicos/{id}").buildAndExpand(topico.getId()).toUri();

        return ResponseEntity.created(uri).body(new TopicoDto(topico));
    }

    @Transactional
    @PutMapping("/{id}")
    @CacheEvict(value = "listaTopicos", allEntries = true)
    public ResponseEntity<DetalhesTopicoDto> atualizar(@PathVariable("id") Long id, @RequestBody @Valid TopicoFormEdicao topicoFormEdicao) {

        Optional<Topico> topicoOptional = topicoRepository.findById(id);

        if (topicoOptional.isPresent()) {
            Topico topico = topicoOptional.get();
            topico.setTitulo(topicoFormEdicao.getTitulo());
            topico.setMensagem(topicoFormEdicao.getMensagem());
            return ResponseEntity.ok(new DetalhesTopicoDto(topico));
        }

        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    @CacheEvict(value = "listaTopicos", allEntries = true)
    public ResponseEntity<?> deletar(@PathVariable("id") Long id) {

        Optional<Topico> topicoOptional = topicoRepository.findById(id);

        if (topicoOptional.isPresent()) {
            topicoRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }
}

package com.estudo.forum.form;

import com.estudo.forum.modelo.Topico;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TopicoFormEdicao {

    @NotNull
    @NotEmpty
    @Length(min = 5)
    private String titulo;

    @NotNull
    @NotEmpty
    @Length(min = 10)
    private String mensagem;

    public Topico toTopico() {
        Topico topico = new Topico();

        BeanUtils.copyProperties(this, topico);

        return topico;
    }

}
